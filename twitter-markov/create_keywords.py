import util
import var
import json
from collections import Counter

def create_whole_counter():
    counter = Counter()
    for file in util.get_all_files(var.count_dir):
        with open(file, 'r', encoding='utf8') as f:
            single_dict = json.load(f)
            single_counter=Counter(single_dict)
        counter.update(single_counter)
    return counter

if __name__ == '__main__':
    counter = create_whole_counter()
    items = counter.most_common()
    available_items = filter(lambda item: item[1] >= 10, items)
    available_words = [item[0] for item in available_items]
    text = '\n'.join(available_words)
    util.write_text(var.keywords_file, text)
