keyword_features = [
    '名詞,一般',
    '名詞,固有名詞',
]

def is_keyword_feature(feature):
    return any(feature.startswith(f) for f in keyword_features)
