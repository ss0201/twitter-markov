import json
import var
import util
import key

def count_keywords(mecab_nodes, counter):
    for node in mecab_nodes:
        try:
            if key.is_keyword_feature(node.feature):
                counter[node.surface] += 1
        except UnicodeDecodeError as e:
            print(f'UnicodeDecodeError: {line}')

def save_count(counter, basename):
    util.create_missing_dir(var.count_dir)
    filepath = f'{var.count_dir}/{basename}'
    with open(filepath, 'w', encoding='utf8') as f:
        json.dump(counter, f)
