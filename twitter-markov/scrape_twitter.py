from twitterscraper import query_tweets, query_tweets_from_user
import var
import util
import os

def scrape(keyword):
    file = f'{var.scraped_dir}/{keyword}.txt'

    if os.path.isfile(file):
        return

    tweets = query_tweets(keyword, 10000)
    tweets_text = '\n'.join([t.text.replace('\n', ' ').replace('\r', '') for t in tweets])

    util.write_text(file, tweets_text)

def main():
    keywords = [
        'アニメ',
        'ラノベ',
        'エロゲー',
        'ソシャゲ',
        'ネトゲ',
        '同人',
        'コミケ',
        '2次元',
        '絵師',
        '萌え',
        '中二病',
        'ツンデレ',
        'ロリ',
    ]

    for keyword in keywords:
        scrape(keyword)

if __name__ == '__main__':
    main()

