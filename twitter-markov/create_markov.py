import markovify
import var
import util

def write_model(model, file):
    model_json = model.to_json()
    util.write_text(file, model_json)

def create_rev_text(text):
    lines = [' '.join(reversed(line.split())) for line in text.splitlines()]
    return '\n'.join(lines)

def create_markov_model(text, outfile):
    model = markovify.NewlineText(text, state_size=4)
    write_model(model, outfile)

def create_markov_models():
    text = util.read_all_files_as_one_text(var.cleansed_dir)
    create_markov_model(text, var.markov_model_file)

    rev_text = create_rev_text(text)
    create_markov_model(rev_text, var.markov_model_rev_file)

if __name__ == '__main__':
    create_markov_models()
