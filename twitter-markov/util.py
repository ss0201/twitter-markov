import os.path
import glob

def create_missing_dir(dir):
    if not os.path.isdir(dir):
        os.makedirs(dir)

def read_text(file):
    with open(file, 'r', encoding='utf8') as f:
        return f.read()

def write_text(file, text):
    create_missing_dir(os.path.dirname(file))
    with open(file, 'w', encoding='utf8') as f:
        f.write(text)

def get_all_files(dir):
    return [f for f in glob.glob(f'{dir}/*.txt')]

def read_all_files_as_one_text(dir):
    files = get_all_files(dir)
    return '\n'.join([read_text(file) for file in files])
