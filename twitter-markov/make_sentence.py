import markovify
import MeCab
import argparse
import math
import var
import util
import key

def load_markov_model(file):
    model_json = util.read_text(file)
    return markovify.NewlineText.from_json(model_json)

def load_keywords():
    keywords_text = util.read_text(var.keywords_file)
    return keywords_text.split()

def extract_keyword(keyphrase, keywords):
    keyword_candidates = []
    mecab = MeCab.Tagger()
    node = mecab.parseToNode(keyphrase)
    while node:
        if key.is_keyword_feature(node.feature):
            keyword_candidates.append(node.surface)
        node = node.next
    
    for keyword in reversed(keyword_candidates):
        if keyword in keywords:
            return keyword
    return ''

def make_sentence_with_keyword(model, model_rev, keyword, max_words):
    max_words_rev = math.floor(max_words * 0.5)
    sentence_rev = model_rev.make_sentence_with_start(keyword, strict=False, max_words=max_words_rev)
    if sentence_rev:
        shared_word_len = model.state_size
        first_words = list(reversed(sentence_rev.split()))
        first_sentence = ' '.join(first_words[:-shared_word_len])
        remaining_word_len = max_words - len(first_words) + shared_word_len
        second_sentence_keywords = ' '.join(first_words[-shared_word_len:])
    else:
        first_sentence = ''
        remaining_word_len = max_words
        second_sentence_keywords = keyword

    second_sentence = model.make_sentence_with_start(second_sentence_keywords, strict=False, max_words=remaining_word_len, test_output=False)
    if second_sentence:
        return ' '.join([first_sentence, second_sentence])
    else:
        return ' '.join([first_sentence, second_sentence_keywords])

def make_sentence(keyphrase, model, model_rev, keywords):
    max_words = 20
    sentence_with_spaces = ''
    keyword = ''

    if keyphrase:
        keyword = extract_keyword(keyphrase, keywords)
        if keyword:
            sentence_with_spaces = make_sentence_with_keyword(model, model_rev, keyword, max_words)
    while not sentence_with_spaces:
        sentence_with_spaces = model.make_sentence(max_words=max_words)

    if sentence_with_spaces:
        return (sentence_with_spaces.replace(' ', ''), keyword)
    else:
        return ('', keyword)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='make a sentence from a markov model.')
    parser.add_argument('--keyphrase',
                       help='a keyphrase to generate a sentence.')
    args = parser.parse_args()

    model = load_markov_model(var.markov_model_file)
    model_rev = load_markov_model(var.markov_model_rev_file)
    keywords = load_keywords()

    (sentence, keyword) = make_sentence(args.keyphrase, model, model_rev, keywords)
    print(f'{keyword} -> {sentence}')
