from http.server import HTTPServer, BaseHTTPRequestHandler
import urllib.parse
import json
import socket
import make_sentence
import var

model = None
model_rev = None
keywords = []

def setup():
    global model, model_rev, keywords
    print(f'Loading markov model...')
    model = make_sentence.load_markov_model(var.markov_model_file)
    print(f'Loading rev markov model...')
    model_rev = make_sentence.load_markov_model(var.markov_model_rev_file)
    print(f'Loading keywords...')
    keywords = make_sentence.load_keywords()

class MarkovHTTPRequestHandler(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()

    def do_GET(self):
        global model, model_rev, keywords

        self._set_headers()

        keyphrase = self.path.split('/')[-1]
        if keyphrase:
            keyphrase = urllib.parse.unquote_plus(keyphrase)
        (sentence, keyword) = make_sentence.make_sentence(keyphrase, model, model_rev, keywords)

        print(f'{keyphrase}:')
        print(f'{keyword} -> {sentence}')
        response = json.dumps({ 'sentence': sentence })
        self.wfile.write(response.encode())

if __name__ == '__main__':
    print(f'Starting server...')

    setup()
    
    ip_address = socket.gethostbyname(socket.gethostname())
    server_address = (ip_address, 8000)
    httpd = HTTPServer(server_address, MarkovHTTPRequestHandler)

    print(f'Server started ({server_address[0]}:{server_address[1]})')

    httpd.serve_forever()
