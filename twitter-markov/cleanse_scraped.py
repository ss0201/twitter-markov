import MeCab
import os.path
import re
from collections import Counter
import count
import util
import var

class Cleanser:
    deleted_texts = [
        # these chars might break markovify
        # https://github.com/jsvine/markovify/issues/84
        r'\(',
        r'\)',
        '[',
        ']',
        r'\"',
        r'\'',
        # end

        r'http\S+',
        r'pic\.twitter\.com\S+',
        r'#\S+',
        r'@\S+',
    ]

    end_chars = [
        '。',
        '？',
        r'\?',
        '！',
        r'\!',
        'ｗ',
        'w',
    ]

    hidden_chars = [
        'ｗ',
        'w',
    ]

    def create_nodes(self, line, mecab):
        nodes = []
        node = mecab.parseToNode(line)
        while node:
            nodes.append(node)
            node = node.next
        return nodes

    def exclude_unreadable_texts(self, text):
        text = self.compiled_deleted_pattern.sub('', text)

        for end_char in self.end_chars:
            text = re.sub(f'{end_char * 2}+', end_char, text)

        return text

    def split_for_markovify(self, nodes):
        splitted_text = ''

        for node in nodes:
            try:
                surface = node.surface
                is_hidden_char = surface in self.hidden_chars

                if not is_hidden_char:
                    splitted_text += surface

                if self.compiled_end_chars_pattern.match(surface):
                    splitted_text += '\n' # reresent sentence by newline
                elif not is_hidden_char:
                    splitted_text += ' ' # split words by space
            except UnicodeDecodeError as e:
                print(f'UnicodeDecodeError: {line}')

        return splitted_text

    def cleanse(self, text):
        readable_text = self.exclude_unreadable_texts(text)

        mecab = MeCab.Tagger()
        counter = Counter()
        splitted_text = ''
        for line in readable_text.splitlines():
            nodes = self.create_nodes(line, mecab)
            count.count_keywords(nodes, counter)
            splitted_text += self.split_for_markovify(nodes)
            splitted_text += '\n'

        return (splitted_text, counter)

    def save_cleansed(self, text, basename):
        file = f'{var.cleansed_dir}/{basename}'
        util.write_text(file, text)

    def compile_patterns(self):
        deleted_pattern = '|'.join(self.deleted_texts)
        self.compiled_deleted_pattern = re.compile(deleted_pattern)

        end_chars_pattern = '|'.join(self.end_chars)
        self.compiled_end_chars_pattern = re.compile(end_chars_pattern)

if __name__ == '__main__':
    cleanser = Cleanser()
    cleanser.compile_patterns()

    for file in util.get_all_files(var.scraped_dir):
        text = util.read_text(file)
        (text, counter) = cleanser.cleanse(text)
        basename = os.path.basename(file)
        cleanser.save_cleansed(text, basename)
        count.save_count(counter, basename)
